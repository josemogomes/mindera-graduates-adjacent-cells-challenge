/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resolusionofpropose;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JO40001636
 */
public class GroupOfList {
    
    List<GroupAdjacentCells> groupList;

    public GroupOfList() {
        this.groupList=new ArrayList<>();
    }

    public void saveGroupList(GroupAdjacentCells groupAC, PrintWriter writeOutPut) {
        if(validarGrupo(groupAC)){
            ReadFileExample1.writeInFile(writeOutPut, groupAC.toString());            
        }
    }

    private boolean validarGrupo(GroupAdjacentCells groupAC) {
        return (groupAC!=null && groupAC.listAdjacentCells.size()>1);        
    }

    @Override
    public String toString() {
        System.out.println("Teste");        
        for(GroupAdjacentCells gl:this.groupList){
            gl.toString();
            System.out.print("\n");
        }
        return "";
    }
}
