/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resolusionofpropose;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JO40001636
 */
public class GroupAdjacentCells {

    List<Cell> listAdjacentCells;

    public GroupAdjacentCells(Cell cell) {
        this.listAdjacentCells = new ArrayList<>();
        this.listAdjacentCells.add(cell);
    }

    public List<Cell> getListAdjacentCells() {
        return listAdjacentCells;
    }

    public void joinTwoGroupsOfAdjacentCells(GroupAdjacentCells otherGroup) {
        this.listAdjacentCells.addAll(otherGroup.getListAdjacentCells());
//        for (Cell cell : otherGroup.getListAdjacentCells()) {
//                this.listAdjacentCells.add(cell);
//        }        
    }

//    public boolean existCellOnGroup(Cell cell) {
//        for (Cell c : this.listAdjacentCells) {
//            if (c.equals(cell)) {
//                return true;
//            }
//        }
//        return false;
//    }

    @Override
    public String toString() {
        boolean flag = false;
        String test = "[ ";
        for (Cell l : this.listAdjacentCells) {
            if (flag) {
                test = test + ", ";
            }
            test = test + l.toString();
            flag = true;
        }
        test = test + " ]";
        return test;
    }
}
