/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resolusionofpropose;

/**
 *
 * @author JO40001636
 */
public class Matrix {
    
    private char[][] matrix;

    public Matrix(int lines, int columns) {
        this.matrix = new char[lines][columns];
    }
    
    public char[][] getMatrix() {
        return this.matrix;
    }
    
    public void setMatrix(char[][] matrix) {
        this.matrix = matrix;
    }
    
    public int getElementMatrix(int lines, int columns) {
        return this.matrix[lines][columns];
    }
    
    public void addElementToMatrix(int lines, int columns, char value){
        this.matrix[lines][columns] = value;
    }
    
    /* Regra de negocio */
    public boolean equalElement(int lines, int columns, char value) {
        return matrix[lines][columns]==value;
    }
    
}
