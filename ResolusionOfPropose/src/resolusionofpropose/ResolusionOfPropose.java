/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resolusionofpropose;

/**
 *
 * @author JO40001636
 */
public class ResolusionOfPropose {

    //private static final String FILENAME1 = "100x100.json";
    //private static final String FILENAME2 = "1000x1000.json";
    //private static final String FILENAME3 = "10000x10000.json";
    private static final String FILENAME4 = "20000x20000.json";
    private static final String OUTPUTFILENAME = "outputfile.txt";    
    private static final int SIZEARRAY = 20000;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String fileName = FILENAME4;
        //Matrix where to save the data loaded from the file.
        Matrix m = new Matrix(SIZEARRAY, SIZEARRAY);
        //1st - Need Import the matrix of cells.
        final long startReadFile = System.nanoTime();
        ReadFileExample1.readFile(fileName, m);
        final long endReadFile = System.nanoTime();
        
        String readFromFile = ("\n1st -> Read the .json file and Build the matrix:" 
                + " Duration: " + (((endReadFile - startReadFile) / 10000000)*(0.01)) + "sec.");        
        System.out.println(readFromFile);
               
        ResolveProblems a = new ResolveProblems();
        //2nd - Aply the algoritm for group cells of 1.
        final long startResolveGroups = System.nanoTime();        
        a.searchGroupsAdjacentCells(m, OUTPUTFILENAME, SIZEARRAY);
        final long endResolveGroups = System.nanoTime();
        
        String timeProcessGroups = ("\n2nd -> Find all groups of adjacent cells in the matix and write in file the output:" 
                + " Duration:" + (((endResolveGroups - startResolveGroups) / 10000000)*(0.01)) + "sec.");
        System.out.println(timeProcessGroups);
        
 /*       
        //3rd - Show the result.
        final long startPrintGroups = System.nanoTime();
        //ReadFileExample1.readAndPrintOutPut(OUTPUTFILENAME);
        final long endPrintGroups = System.nanoTime();        
        
        String timePrintTheResult = ("\n3rd -> Print all groups of adjacent cells found:" 
                + " Duration:" + ((endPrintGroups - startPrintGroups) / 1000000000) + "sec.");
        System.out.println(timePrintTheResult);        
  */    
    }  

}
