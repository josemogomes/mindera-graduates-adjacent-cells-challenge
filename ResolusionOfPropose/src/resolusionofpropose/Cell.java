/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resolusionofpropose;

/**
 *
 * @author JO40001636
 */
public class Cell {
    
    private final int line;
    private final int column;

    public Cell(int line, int column) {
        this.line = line;
        this.column = column;
    }

    @Override
    public String toString() {
        return "[" + line + "," + column + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Cell otherCell = (Cell) obj;
        
        return (this.line==otherCell.line && this.column==otherCell.column);        
    }    
    
}
