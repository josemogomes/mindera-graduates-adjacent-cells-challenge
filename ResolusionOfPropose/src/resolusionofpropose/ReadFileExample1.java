package resolusionofpropose;

/**
 *
 * @author JO40001636
 * @link https://www.mkyong.com/java/how-to-read-file-from-java-bufferedreader-example/
 * @link https://stackoverflow.com/questions/5993779/use-string-split-with-multiple-delimiters?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReadFileExample1 {

    public static void readFile(String fileName, Matrix m) {
        BufferedReader br = null;
        FileReader fr = null;
        try {
            fr = new FileReader(fileName);
            br = new BufferedReader(fr);
            String sCurrentLine;
            int lineIndex = 0;
            while ((sCurrentLine = br.readLine()) != null) {
                int columnIndex = 0;
                boolean flag = false;
                for (int i = 0; i < sCurrentLine.length(); i++) {
                    char caracter = sCurrentLine.charAt(i);
                    if (caracter == '1' || caracter == '0') {
                        m.addElementToMatrix(lineIndex, columnIndex, caracter);
                        columnIndex++;
                        flag = true;
                    }
                }
                if (flag) {
                    lineIndex++;
                }
            }
        } catch (IOException e) {
            Logger.getLogger(ReadFileExample1.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReadFileExample1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static PrintWriter openFileToWrite(String fileName) {
        PrintWriter write = null;
        try {
            write = new PrintWriter(fileName, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(ReadFileExample1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return write;
    }

    public static void writeInFile(PrintWriter write, String textToSave) {
        write.println(textToSave);
    }

    public static boolean closeFileToWrite(PrintWriter write) {
        if (write != null) {
            write.close();
            return true;
        } else {
            return false;
        }
    }

    public static void readAndPrintOutPut(String fileName) {
        BufferedReader br = null;
        FileReader fr = null;
        try {
            fr = new FileReader(fileName);
            br = new BufferedReader(fr);
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
            }
        } catch (IOException e) {
            Logger.getLogger(ReadFileExample1.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReadFileExample1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
