/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resolusionofpropose;

import java.io.PrintWriter;

/**
 *
 * @author JO40001636
 */
public class ResolveProblems {

    public void searchGroupsAdjacentCells(Matrix m, String outputFileName, int size) {

        GroupOfList gl = new GroupOfList();
        PrintWriter writeOutPut = ReadFileExample1.openFileToWrite(outputFileName);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (m.getElementMatrix(i, j) == '1') {
                    //start of group search.                    
                    gl.saveGroupList(analisarVizinhos(m, i, j, size, size), writeOutPut);
                }
            }
        }
        ReadFileExample1.closeFileToWrite(writeOutPut);
    }

    public GroupAdjacentCells analisarVizinhos(Matrix m, int line, int column, int sizeLines, int sizeColumns) {

        GroupAdjacentCells novoGrupo = new GroupAdjacentCells(new Cell(line, column));
        m.addElementToMatrix(line, column, '0');//if is visiti change to zero.
        //Norte
        if ((line - 1) >= 0 && m.getElementMatrix((line - 1), column) == '1') {
            novoGrupo.joinTwoGroupsOfAdjacentCells(analisarVizinhos(m, (line - 1), column, sizeLines, sizeColumns));
        }
        //Este
        if ((column + 1) < sizeColumns && m.getElementMatrix(line, (column + 1)) == '1') {
            novoGrupo.joinTwoGroupsOfAdjacentCells(analisarVizinhos(m, line, (column + 1), sizeLines, sizeColumns));
        }
        //Sul
        if ((line + 1) < sizeLines && m.getElementMatrix((line + 1), column) == '1') {
            novoGrupo.joinTwoGroupsOfAdjacentCells(analisarVizinhos(m, (line + 1), column, sizeLines, sizeColumns));
        }
        //Oeste
        if ((column - 1) >= 0 && m.getElementMatrix(line, (column - 1)) == '1') {
            novoGrupo.joinTwoGroupsOfAdjacentCells(analisarVizinhos(m, line, (column - 1), sizeLines, sizeColumns));
        }
        return novoGrupo;
    }
}
